angular.module('starter.services', ['ngResource'])


.service('HttpService', function($http) {
 return {
  getWarden: function() {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.get('http://52.63.198.5:8080/wardenjson')
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Warden', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		   alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
    getWarning: function() {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.get('http://52.63.198.5:8080/warden_warning_json')
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Warden', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		  alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
   getLocation: function() {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.get('http://52.63.198.5:8080/timesheet/get-all-locations-codes')
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Location', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		   alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
   getRollcall: function(timeSearchDto) {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.post('http://52.63.198.5:8080/warden-rollcall-search-employee',timeSearchDto)
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Location', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		   alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
   persistWarden: function(wardenRollcallDto) {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.post('http://52.63.198.5:8080/warden-persist-warden-rollcall-mobile',wardenRollcallDto)
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Location', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		   alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
   updateWarden: function(wardenRollcallDto) {
     // $http returns a promise, which has a then function, which also returns a promise.
     return $http.post('http://52.63.198.5:8080/warden-update-warden-rollcall-mobile',wardenRollcallDto)
       .then(function(response) {
         // In the response, resp.data contains the result. Check the console to see all of the data returned.
         //console.log('Get Location', response);
         return response.data;
      }, function errorCallback(response) {
		  //console.log("service: ",response);
		   alert("Network Error");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
   },
 };
})

.service('EmployeeInfo', function() {
 return {
   detail: {},
   getDetail: function() {
     return this.detail;
   },
   updateDetail:function(warden){
	   this.detail=warden;
   }
 }
})


.service('WarningInfo', function() {
 return {
   detail: {},
   getDetail: function() {
     return this.detail;
   },
   updateDetail:function(warning){
	   this.detail=warning;
   }
 }
})

.factory('Rollcall', function ($resource) {
    return $resource('http://52.63.198.5:8080/warden-rollcall-search-employee');
})




.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'Employee seems to still be on break',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Employee has not Clocked In yet',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'Employee has not Clocked Off yet',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Employee has not Clocked Off yet',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'Employee seems to still be on break',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});






