angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('EmployeeDetailCtrl', function($scope,EmployeeInfo) {
  $scope.employee = EmployeeInfo.getDetail();
  
  function onSuccess(result){
  console.log("Success:"+result);
}

function onError(result) {
 console.log("Error:"+result);
}

$scope.callNumber = function(number){
  console.log("Launching Calling Service for number "+number);
  window.plugins.CallNumber.callNumber(onSuccess, onError, number, false);
}
  
})

.controller('OnsiteCtrl', function($scope,$state,HttpService,EmployeeInfo,$ionicLoading) {
 
 
//$scope.wardens ={"listofWardenDto":[{"empNo":"100201","fname":"Rachael","lname":"PRICE","timeIn":"7:00","deviceName":"Time Synergy Front Device","location":"SYD","reportsTo":"Jones,Chris","onBreak":"","empPosition":"General Manager","empMobile":"87654321","reportsToPosition":"Genaral Manager","reportsToMobile":"12345678"}],"u":"TSADMIN"};

$scope.loadOnsite =function(){
	$ionicLoading.show({
template: 'Loading...'
});
 HttpService.getWarden()
 .then(function(response) {
	 
   $scope.wardens = response;   
   $ionicLoading.hide();
 });
};

$scope.doRefresh = function() {
 
 HttpService.getWarden()
 .then(function(response) {
	 
   $scope.wardens = response;
   $scope.$broadcast('scroll.refreshComplete');
 });
 
};

$scope.showDetails =function(warden){
	EmployeeInfo.updateDetail(warden);
	$state.go('tab.employee-detail');
};
})

.controller('WarningCtrl', function($scope,$state,HttpService,WarningInfo,$ionicLoading) {

 
//$scope.warning ={"u":"TSADMIN","listOfWardenWarningDto":[{"empNo":"100301","fname":"Martha","lname":"MORGAN","location":"Sydney","reportsTo":"Snow,John","empPosition":"TimeSynergy Testing","empMobile":"56781234","reportsToPosition":"General Manager","reportsToMobile":"0415 789 363","action":"Start Shift - 11:00","warning":"Employee has not Clocked In yet"},{"empNo":"100401","fname":"George","lname":"NEWTON","location":"Sydney","reportsTo":"Singh,Simaran","empPosition":"Bartender","empMobile":"87654321","reportsToPosition":"Bar Manager","reportsToMobile":"0417 622 257","action":"Start Shift - 11:00","warning":"Employee has not Clocked In yet"},{"empNo":"100201","fname":"Rachael","lname":"PRICE","location":"Sydney","reportsTo":"Jones,Chris","empPosition":"General Manager","empMobile":"12345678","reportsToPosition":"General Manager","reportsToMobile":"87654321","action":"Start Shift - 11:00","warning":"Employee has not Clocked In yet"}]};

$scope.loadWarning =function(){
	$ionicLoading.show({
template: 'Loading...'
});
 HttpService.getWarning()
 .then(function(response) {
	 
   $scope.warnings = response;
   $ionicLoading.hide();
 });
};

$scope.doRefresh = function() {
 
 HttpService.getWarning()
 .then(function(response) {
	 
   $scope.warnings = response;   
  $scope.$broadcast('scroll.refreshComplete');
 });
 
};

$scope.showWarningDetails =function(warning){
	WarningInfo.updateDetail(warning);
	$state.go('tab.warning-detail');
};
})

.controller('WarningDetailCtrl', function($scope,WarningInfo) {
  $scope.warning = WarningInfo.getDetail();
  
  function onSuccess(result){
  console.log("Success:"+result);
}

function onError(result) {
 console.log("Error:"+result);
}

$scope.callNumber = function(number){
  console.log("Launching Calling Service for number "+number);
  window.plugins.CallNumber.callNumber(onSuccess, onError, number, false);
}
  
})


.controller('RollcallCtrl', function($scope,$state,HttpService,$ionicLoading) {
  $scope.timesheetSearchDto={};
  $scope.locationsCodes=[];
  $scope.wardenRollcallDtos=[];
//$scope.locationsCodes=["NEW","100","200","PHKT","MEL","BRIS","SYD"];

var loadLocationCode =function(){
	 $ionicLoading.show({
template: 'Loading...'
});
 HttpService.getLocation()
 .then(function(response) {
	 
    $scope.locationsCodes = response;
    $ionicLoading.hide();
 });
};

$scope.getRollcall = function(){
	
	$ionicLoading.show({
template: 'Loading...'
});
 HttpService.getRollcall($scope.timesheetSearchDto)
 .then(function(response) {
	 //console.log(response);
    if (typeof response === 'string' || response instanceof String) {
                            if (response === 'Selected Location code could not be nulll') {
                                alert('Please select location code');
                               $ionicLoading.hide();
                            } else if (response === 'No Onsite Employees') {
                                alert('No Onsite Employee');
                                $ionicLoading.hide();
                            }
                            else if (response === 'No Onsite Employees for Selected Location') {
                                alert('No Onsite Employees for Selected Location');
                               $ionicLoading.hide();
                            }else {
                                alert(response);
                               $ionicLoading.hide();
                            }
                        } else if (response !== undefined && response.length !==0) {
                           
                            $scope.wardenRollcallDtos = response;
                            $ionicLoading.hide();
                        }
 });
};

$scope.persistEvac = function(wardenRollcallDto){
           wardenRollcallDto.status="Evacuated";
           $scope.persistWarden(wardenRollcallDto);
       };
       
       $scope.persistMissing = function(wardenRollcallDto){
           wardenRollcallDto.status="Missing";
            $scope.persistWarden(wardenRollcallDto);
       };
       
        $scope.updateToEvac = function(wardenRollcallDto){
           wardenRollcallDto.status="Evacuated";
            $scope.updateWarden(wardenRollcallDto);
       };
       
       $scope.updateToMissing = function(wardenRollcallDto){
           wardenRollcallDto.status="Missing";
           
            $scope.updateWarden(wardenRollcallDto);
       };
	   
$scope.persistWarden = function(wardenRollcallDto){
	
	$ionicLoading.show({
template: 'Saving...'
});
 HttpService.persistWarden(wardenRollcallDto)
 .then(function(response) {
	 console.log(response);
	 angular.copy(response,wardenRollcallDto)
     $ionicLoading.hide();
 });
};  

$scope.updateWarden = function(wardenRollcallDto){

 HttpService.updateWarden(wardenRollcallDto)
 .then(function(response) {
	
	 angular.copy(response,wardenRollcallDto)
   
 });
};  

$scope.doRefresh = function() {
 loadLocationCode();
  HttpService.getRollcall($scope.timesheetSearchDto)
 .then(function(response) {
	 //console.log(response);
    if (typeof response === 'string' || response instanceof String) {
                            if (response === 'Selected Location code could not be nulll') {
                                alert('Please select location code');
                             
                            } else if (response === 'No Onsite Employees') {
                                alert('No Onsite Employee');
                               
                            }
                            else if (response === 'No Onsite Employees for Selected Location') {
                                alert('No Onsite Employees for Selected Location');
                              
                            }else {
                                alert(response);
                              
                            }
                        } else if (response !== undefined && response.length !==0) {
                           
                            $scope.wardenRollcallDtos = response;
                           
                        }
 });
  $scope.$broadcast('scroll.refreshComplete');
};

loadLocationCode();

})




.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
